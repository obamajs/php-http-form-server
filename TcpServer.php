<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/11/13 0013
 * Time: 8:34
 */

class  TcpServer
{
    private $port = 81;
    private $addr = "127.0.0.1";
    private $socket_handle;
    private $back_log = 10;
    private $http_header_parsed = false;
    private $http_headers = [];
    private $multiple_part_enabled = false;
    private $http_boundary;
    private $boundary_start_matched = false;
    private $internal_buffer = "";
    private $form_part_field_matched = false;
    private $http_form_data = [];
    private $current_field_name;
    private $current_file_name;
    private $field_content_type_checked = false;
    private $current_field_content_type = '';
    private $separator_checked = false;
    private $start_match_field_content = false;
    private $field_type_is_file = false;
    private $has_read_bytes_num = 0;
    private $file_content = '';
    private $last_offset = 0;
    private $content_length;
    private $request_line_parsed = false;
    private $is_normal_data = true;
    private $http_content_has_read_bytes = 0;

    private $request_method;
    private $uri;

    public function __construct($port = 81, $addr = "127.0.0.1", $back_log = 10)
    {
        $this->port = $port;
        $this->addr = $addr;
        $this->back_log = $back_log;
    }


    /**
     * @throws Exception
     */
    private function createSocket()
    {
        //创建socket套接字
        $this->socket_handle = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        if (!$this->socket_handle) {
            //创建失败抛出异常，socket_last_error获取最后一次socket操作错误码，socket_strerror打印出对应错误码所对应的可读性描述
            throw new Exception(socket_strerror(socket_last_error($this->socket_handle)));
        } else {
            echo "create socket successful\n";
        }
    }


    /**
     * @throws Exception
     */
    private function bindAddr()
    {
        if (!socket_bind($this->socket_handle, $this->addr, $this->port)) {
            throw new Exception(socket_strerror(socket_last_error($this->socket_handle)));
        } else {
            echo "bind addr successful\n";
        }
    }

    private function listen()
    {
        if (!socket_listen($this->socket_handle, $this->back_log)) {
            throw new Exception(socket_strerror(socket_last_error($this->socket_handle)));
        } else {
            echo "socket  listen successful\n";
        }
    }

    /**
     * @throws Exception
     */
    private function accept()
    {
        $client_socket_handle = socket_accept($this->socket_handle);
        if (!$client_socket_handle) {
            echo "socket_accept call failed\n";
            exit(1);
        } else {
            while (true) {
                //每一次读取2048字节的数据
                $bytes_num = socket_recv($client_socket_handle, $buffer, 2048, 0);
                $this->internal_buffer .= $buffer;
                if (!$bytes_num) {
                    echo "socket_recv  failed\n";
                    exit(1);
                } else {
                    echo "read num:" . $bytes_num . "\n";
                    if (!$this->http_header_parsed) {
                        while (true) {
                            if (preg_match("/([^\r]*)\r\n/", $this->internal_buffer, $match) > 0) {
                                if (empty($match[1])) {
                                    //http 头部匹配完成
                                    $this->http_header_parsed = true;
                                } else {
                                    if (!$this->request_line_parsed) {
                                        $this->request_line_parsed = true;
                                        $line_parts = explode(" ", $match[1]);
                                        $this->request_method = trim($line_parts[0], " ");
                                        $this->uri = $line_parts[1];
                                    } else {
                                        $parts = explode(":", $match[1]);
                                        $key = $parts[0];
                                        $value = trim($parts[1], " ");//value的左侧可能有空格
                                        $this->http_headers[$key] = trim($value, "\r\n ");
                                        if (strcmp($key, "Content-Type") == 0) {
                                            //检查内容的类型
                                            $multipart_type = "multipart/form-data";
                                            //如果有上传文件的话
                                            if (strncmp($multipart_type, $value, strlen($multipart_type)) == 0) {
                                                $this->multiple_part_enabled = true;
                                                //获取边界也就是boundary
                                                $this->http_boundary = explode("=", trim(substr($value, strlen($multipart_type) + 1), " "))[1];
                                            } else {
                                                //此时内容类型为application/x-www-form-urlencoded
                                            }
                                        } else if (strcmp("Content-Length", $key) == 0) {
                                            //检查内容的长度
                                            $this->content_length = intval($value);
                                        }
                                    }
                                }
                                $this->internal_buffer = substr($this->internal_buffer, strlen($match[0]));
                            } else {
                                break;
                            }
                            if ($this->http_header_parsed) {
                                break;
                            }
                        }
                    }
                    if (!$this->http_header_parsed) {
                        continue;
                    }
                    if (strlen($this->internal_buffer) == 0) {
                        continue;
                    }

                    if ($this->http_header_parsed) {
                        if (strpos($this->request_method, "GET") === false) {
                            if ($this->multiple_part_enabled) {
                                //检测boundary
                                while (true) {
                                    if (!$this->start_match_field_content) {
                                        if (preg_match("/([^\r]*)\r\n/", $this->internal_buffer, $match) > 0) {
                                            if (!$this->boundary_start_matched) {
                                                $this->boundary_start_matched = true;
                                            }
                                            if (strncmp($match[1], "Content-Disposition:", strlen('Content-Disposition:')) == 0) {
                                                $disposition_parts = explode(';', str_replace(" ", "", substr($match[1], strlen("Content-Disposition:"))));
                                                $this->current_field_name = str_replace("\"", "", substr($disposition_parts[1], 5));
                                                if (count($disposition_parts) > 2) {
                                                    $this->current_file_name = str_replace("\"", "", substr(trim($disposition_parts[2], " "), strlen("filename=")));
                                                }
                                            }
                                            if (strncmp($match[1], "Content-Type:", strlen('Content-Type:')) == 0) {
                                                $content_value = str_replace(" ", "", substr($match[1], strlen("Content-Type:")));
                                                if (($pos = strpos($content_value, ";")) > 0) {
                                                    $this->current_field_content_type = substr($content_value, 0, $pos);
                                                } else {
                                                    $this->current_field_content_type = $content_value;
                                                }
                                            }
                                            if (empty($match[1])) {
                                                $this->start_match_field_content = true;
                                                if (strpos($this->current_field_content_type, "text") !== false || empty($this->current_field_content_type)) {
                                                    $this->field_type_is_file = false;
                                                } else {
                                                    $this->field_type_is_file = true;
                                                }
                                            }
                                            $this->has_read_bytes_num += strlen($match[0]);
                                            $this->internal_buffer = substr($this->internal_buffer, strlen($match[0]));
                                        } else {
                                            break;
                                        }
                                    } else {
                                        if (preg_match("/\r\n--{$this->http_boundary}(--)?\r\n/", $this->internal_buffer, $match, PREG_OFFSET_CAPTURE, 0) > 0) {
                                            if ($this->field_type_is_file) {
                                                file_put_contents(__DIR__ . '/' . $this->current_file_name, substr($this->internal_buffer, 0, $match[0][1]));
                                            } else {
                                                $this->http_form_data[$this->current_field_name] = substr($this->internal_buffer, 0, $match[0][1]);
                                            }
                                            $this->internal_buffer = substr($this->internal_buffer, $match[0][1] + 2);
                                            $this->has_read_bytes_num += ($match[0][1] + 2);
                                            if (($this->content_length - $this->has_read_bytes_num) == (strlen($this->http_boundary) + 6)) {
                                                echo "client content parsed finished\n";
                                                //http内容解析完成
                                                foreach ($this->http_form_data as $key => $value) {
                                                    echo $key . "=>" . $value . "\n";
                                                }
                                            } else {
                                                $this->start_match_field_content = false;
                                                $this->boundary_start_matched = false;
                                                $this->form_part_field_matched = false;
                                                $this->field_content_type_checked = false;
                                                $this->current_field_name = null;
                                                $this->current_file_name = null;
                                                $this->current_field_content_type = null;
                                                $this->field_type_is_file = false;
                                            }
                                        } else {
                                            break;
                                        }
                                    }
                                }
                            } else {
                                if (strlen($this->internal_buffer) < $this->content_length) {
                                    //还有数据没有读取，不进行任何操作
                                } else {
                                    foreach (explode('&', $this->internal_buffer) as $pair) {
                                        $pair_parts = explode('=', $pair);
                                        $this->http_form_data[urldecode($pair_parts[0])] = urldecode($pair_parts[1]);
                                    }

                                    foreach ($this->http_form_data as $key => $value) {
                                        echo $key . "=>" . $value . "\n";
                                    }
                                }
                            }
                        } else {
                            echo "GET request not supported";
                        }
                    }
                }
            }
        }
    }

    public function startServer()
    {
        try {
            $this->createSocket();
            $this->bindAddr();
            $this->listen();
            $this->accept();
        } catch (Exception $exception) {
            echo $exception->getMessage() . "\n";
        }
    }
}

$server = new TcpServer();
$server->startServer();
